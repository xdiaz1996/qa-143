from .dependency import find_main, get_address, find_node_by_tag


def get_dependency_parse(question):
    return question["dep"]


def get_constituency_parse(question):
    return question["par"]


def get_question_text(question):
    return question["text"]


def get_story_id(question):
    return question["sid"]


def get_question_id(question):
    return question["qid"]


def get_question_difficulty(question):
    return question["difficulty"]


def get_question_type(question):
    return question["type"]


def get_question_category(question):
    question_text = get_question_text(question)

    if ", " in question_text:
        return question_text.split(", ")[1].split(" ")[0].lower()
    else:
        return question_text.split(" ")[0].lower()


def base_verb_patterns(question_dependency_parse, dependency_main):
    verbs = {}

    if "conj" in dependency_main["deps"]:
        conj_address = dependency_main["deps"]["conj"][0]
        conj_node = question_dependency_parse.get_by_address(conj_address)
        verbs["base"] = conj_node["word"]
    elif "advcl" in dependency_main["deps"]:
        advcl_address = dependency_main["deps"]["advcl"][0]
        advcl_node = question_dependency_parse.get_by_address(advcl_address)

        if advcl_node["tag"][0] == "V":
            verbs["base"] = advcl_node["word"]
        else:
            verbs["base"] = dependency_main["word"]
    else:
        verbs["base"] = dependency_main["word"]
    return verbs


def past_tense_verb_patterns(question_dependency_parse, dependency_main):
    verbs = {}

    if "advcl" in dependency_main["deps"]:
        advcl_address = dependency_main["deps"]["advcl"][0]
        advcl_node = question_dependency_parse.get_by_address(advcl_address)
        verbs["past_tense"] = advcl_node["word"]
    else:
        return_word = dependency_main["word"]
        if return_word.lower() in ["who", "what", "where", "which", "did", "why"]:
            verbs["base"] = None
            verbs["past_tense"] = None
            verbs["past_participle"] = None
        else:
            verbs["past_tense"] = dependency_main["word"]
    return verbs


def past_participle_verb_patterns(question_dependency_parse, dependency_main):
    return {"past_participle": dependency_main["word"]}


def get_question_verbs(question):
    question_dependency_parse = get_dependency_parse(question)
    dependency_main = find_main(question_dependency_parse)
    
    # print(dependency_main)
    if dependency_main["tag"] == "VB":
        return base_verb_patterns(question_dependency_parse, dependency_main)
    elif dependency_main["tag"] == "VBN":
        return past_participle_verb_patterns(question_dependency_parse, dependency_main)
    elif dependency_main["tag"] == "VBD":
        return past_tense_verb_patterns(question_dependency_parse, dependency_main)
    else:
        if dependency_main["tag"][0] == "N":
            acl_address = dependency_main["deps"]["acl:relcl"][0]
            acl_node = question_dependency_parse.get_by_address(acl_address)

            return {"base": acl_node["word"]}
        else:
            return {"base": None ,"past_tense": None, "past_participle": None}


def get_question_nouns(question):
    nouns = {}
    question_dependency_parse = get_dependency_parse(question)
    dependency_main = find_main(question_dependency_parse)
    if "nsubj" in dependency_main["deps"]:
        nsubj_address = dependency_main["deps"]["nsubj"][0]
        nsubj_node = question_dependency_parse.get_by_address(nsubj_address)
        # print(">>>>>>", nsubj_node)
        if nsubj_node["tag"][0] == "N" or nsubj_node["tag"] == "PRP":
            nouns["noun_subject"] = nsubj_node["word"]
        else:
            if "advcl" in dependency_main["deps"]:
                advcl_address = dependency_main["deps"]["advcl"][0]
                advcl_node = question_dependency_parse.get_by_address(advcl_address)
                if "nsubj" in advcl_node["deps"]:
                    nsubj_address = advcl_node["deps"]["nsubj"][0]
                    nsubj_node = question_dependency_parse.get_by_address(nsubj_address)
                    nouns["noun_subject"] = nsubj_node["word"]
            else:
                nouns["noun_subject"] = None
    else:
        nouns["noun_subject"] = None
    return nouns
