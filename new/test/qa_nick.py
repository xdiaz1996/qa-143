import sys, nltk, operator, re
from qa_engine.base import QABase
from qa_engine.score_answers import main as score_answers
from nltk.stem.wordnet import WordNetLemmatizer
#import tsv
#writer = tsv.TsvWriter(open("hw6-responses.tsv", "w"))
#i = 0

# baseline funct
def get_sentences(text):
    sentences = nltk.sent_tokenize(text)
    sentences = [nltk.word_tokenize(sent) for sent in sentences]
    sentences = [nltk.pos_tag(sent) for sent in sentences]
    
    return sentences
#baseline funct
def get_bow(tagged_tokens, stopwords):
    return set([t[0].lower() for t in tagged_tokens if t[0].lower() not in stopwords])

# See if our pattern matches the current root of the tree
def matches(pattern, root):
    # Base cases to exit our recursion
    # If both nodes are null we've matched everything so far
    if root is None and pattern is None: 
        return root
        
    # We've matched everything in the pattern we're supposed to (we can ignore the extra
    # nodes in the main tree for now)
    elif pattern is None:                
        return root
        
    # We still have something in our pattern, but there's nothing to match in the tree
    elif root is None:                   
        return None

    # A node in a tree can either be a string (if it is a leaf) or node
    plabel = pattern if isinstance(pattern, str) else pattern.label()
    rlabel = root if isinstance(root, str) else root.label()

    # If our pattern label is the * then match no matter what
    if plabel == "*":
        return root
    # Otherwise they labels need to match
    elif plabel == rlabel:
        # If there is a match we need to check that all the children match
        # Minor bug (what happens if the pattern has more children than the tree)
        for pchild, rchild in zip(pattern, root):
            match = matches(pchild, rchild) 
            if match is None:
                return None 
        return root
    
    return None

def pattern_matcher(pattern, tree):
    for subtree in tree.subtrees():
        node = matches(pattern, subtree)
        if node is not None:
            return node
    return None

#baseline funct
def baseline(qbow, sentences, stopwords, tree):
    # Collect all the candidate answers
    answers = []
    tree_answers = []
    i=0
    for sent in sentences:
        # A list of all the word tokens in the sentence
        sbow = get_bow(sent, stopwords)
        
        # Count the # of overlapping words between the Q and the A
        # & is the set intersection operator
        overlap = len(qbow & sbow)
        
        answers.append((overlap, sent))
        tree_answers.append((overlap, tree[i]))
        #print(tree[i])
        i+= 1
    #print("------------------------------------")  
    # Sort the results by the first element of the tuple (i.e., the count)
    # Sort answers from smallest to largest by default, so reverse it
    answers = sorted(answers, key=operator.itemgetter(0), reverse=True)
    tree_answers = sorted(tree_answers, key=operator.itemgetter(0), reverse=True)
    # Return the best answer
    #best_answer = (answers[0])[1]    
    best_answer = []
    best_tree = []
    for answer in answers:
        if(answer[0] == answers[0][0]):
            best_answer.extend(answer[1])
    for answer in tree_answers:
        #print(answer[1])
        if(answer[0] == answers[0][0]):
            best_tree.extend(answer[1])
    return best_answer, best_tree

#baseline funct
def find_phrase(tagged_tokens, qbow):
    for i in range(len(tagged_tokens) - 1, 0, -1):
        word = (tagged_tokens[i])[0]
        if word in qbow:
            return tagged_tokens[i+1:]

def whoProc(question, answer):
    toksent = nltk.sent_tokenize(question)
    tokword = [nltk.word_tokenize(sent) for sent in toksent]
    modq = ""
    for word in tokword[0]:
        if word == "Who":
            continue
        if word == "?":
            continue
        modq = modq +" "+ str(word)
    whoregex = r"(.*)"+re.escape(modq)
    newans = re.findall(whoregex, answer)
    if newans == []:
        answer = answer
    else:
        answer = newans[0]
    return answer

def whereProc(question, answer):
    toksent = nltk.sent_tokenize(question)
    tokword = [nltk.word_tokenize(sent) for sent in toksent]
    verb = ""
    tagged = nltk.pos_tag(tokword[0])
    for word in tagged:
        checkv = ""
        checkv = word[1]
        if checkv[:2] == "VB":
            verb = word[0]
        checkv = word[0]
        if checkv[-3:] == "ing":
            verb = word[0]
    if verb == "":
        return answer
    lmtzr = WordNetLemmatizer()
    verb = lmtzr.lemmatize(verb, "v")
    ansent = nltk.sent_tokenize(answer)
    shorta = ""
    i = 0
    for sent in ansent:
        tokwrd = [nltk.word_tokenize(sent) for sent in ansent]
        tag = nltk.pos_tag(tokwrd[i])
        for word in tag:
            cmatch = ""
            cmatch = lmtzr.lemmatize(word[0], "v")
            if cmatch == verb:
                shorta = shorta +" "+sent
                break
        i = i + 1
    #print(question+"\n")
    #print(answer+"\n")
    #print(shorta)
    #sys.exit(0)
    if shorta == "":
        answer = answer
    else:
        answer = shorta
    return answer

def whatProc(question, answer):
    #essentialy looks for a
    toksent = nltk.sent_tokenize(question)
    tokword = [nltk.word_tokenize(sent) for sent in toksent]
    verb = ""
    tagged = nltk.pos_tag(tokword[0])
    for word in tagged:
        checkv = ""
        checkv = word[1]
        if checkv[:2] == "VB":
            verb = word[0]
        if checkv[:2] == "NN":
            verb = word[0]
        checkv = word[0]
        if checkv[-3:] == "ing":
            verb = word[0]
        if checkv[-2:] == "ed":
            verb = word[0]
    if verb == "":
        return answer
    lmtzr = WordNetLemmatizer()
    verb = lmtzr.lemmatize(verb, "v")
    ansent = nltk.sent_tokenize(answer)
    shorta = ""
    i = 0
    for sent in ansent:
        tokwrd = [nltk.word_tokenize(sent) for sent in ansent]
        tag = nltk.pos_tag(tokwrd[i])
        for word in tag:
            cmatch = ""
            cmatch = lmtzr.lemmatize(word[0], "v")
            if cmatch == verb:
                shorta = shorta +" "+sent
                break
        i = i + 1
    #print(question+"\n")
    #print(answer+"\n")
    #print(shorta)
    if shorta == "":
        answer = answer
    else:
        answer = shorta
    return answer

def get_answer(question, story):
    """
    :param question: dict
    :param story: dict
    :return: str


    question is a dictionary with keys:
        dep -- A list of dependency graphs for the question sentence.
        par -- A list of constituency parses for the question sentence.
        text -- The raw text of story.
        sid --  The story id.
        difficulty -- easy, medium, or hard
        type -- whether you need to use the 'sch' or 'story' versions
                of the .
        qid  --  The id of the question.


    story is a dictionary with keys:
        story_dep -- list of dependency graphs for each sentence of
                    the story version.
        sch_dep -- list of dependency graphs for each sentence of
                    the sch version.
        sch_par -- list of constituency parses for each sentence of
                    the sch version.
        story_par -- list of constituency parses for each sentence of
                    the story version.
        sch --  the raw text for the sch version.
        text -- the raw text for the story version.
        sid --  the story id


    """
    ###     Your Code Goes Here         ###

    #-baseline method-------------------------------------------
    answer = []
    version = question["type"]
    if version == "Story":
        text = story["text"]
        tree = story["story_par"]
    else:
        text = story["sch"]
        tree = story["sch_par"]
    q = question["text"]
    #print(q)
    #print("question: ", question)
    stopwords = set(nltk.corpus.stopwords.words("english"))
    qbow = get_bow(get_sentences(q)[0], stopwords)
    sentences = get_sentences(text)
    answer_stuff, trees = baseline(qbow, sentences, stopwords,tree)
    for t in answer_stuff:
        answer.append(t[0])
    answer = " ".join(answer)
    
    if q[:3] == "Who" or q[:3] == "who":
        answer = whoProc(q, answer)
        if q == "Who is the story about?":
            words=[]
            for sent in sentences:
                for i in sent:
                    if i[1]=="NNP" or i[1]=="NN":
                        words.append(i[0])
            word = nltk.FreqDist(words)
            words = word.most_common(2)
            answer = "the " + words[0][0] + " and " + words[1][0]
    elif q[:5] == "Where" or q[:5] == "where":
        answer = whereProc(q, answer)
    elif q[:4] == "What" or q[:4] == "what":
        answer = whatProc(q, answer)
    elif q[:4] == "When":
        answer = answer.split(".")
        answer = answer[0]
        answer = answer.split(",")
        answer = answer[0]
    elif q[:3] == "Why":
        if "because" in answer:
            pattern = nltk.ParentedTree.fromstring("(SBAR)")
            sub = []
            for tree in trees:
                subtree = pattern_matcher(pattern, tree)
                if subtree is not None:
                    check = " ".join(subtree.leaves())
                    if "because" in check:
                        sub.append(check)
            if sub:
                answer = " ".join(sub)
        else:
            q2 = q.split(" ")
            q2 = q2[-1]
            answer = answer.split(q2[:-1])
            answer = answer[-1]
    elif q[:3] == "How":
        q2 = q.split(" ")
        q2 = q2[-1]
        answer = answer.split(q2[:-1])
        answer = answer[-1]
    elif q[:3] == "Had":
        answer = "no"
    elif q[:3] == "Did":
        answer = "yes"
    elif q[:5] == "Which":
        pattern = nltk.ParentedTree.fromstring("(VP (*) (NP))")
        subtree = pattern_matcher(pattern, trees[0])
        if subtree is not None:
            pattern = nltk.ParentedTree.fromstring("(NP)")
            subtree2 = pattern_matcher(pattern, subtree)
            if subtree2 is not None:
                    answer = " ".join(subtree2.leaves())
        else:
            pattern = nltk.ParentedTree.fromstring("(NP)")
            subtree = pattern_matcher(pattern, trees[0])
            if subtree is not None:
                    answer = " ".join(subtree.leaves())
    
    


    #print("answer: "+format(answer))
    #print("answer: "," ".join(t[0]for t in answer_stuff))
    #answer = " ".join(t[0]for t in answer_stuff) 
    #global writer
    #writer.line(answer, qid)
    #-end of baseline method--------------------------------------
    
    ###     End of Your Code         ###
    return answer



#############################################################
###     Dont change the code in this section
#############################################################
class QAEngine(QABase):
    @staticmethod
    def answer_question(question, story):
        answer = get_answer(question, story)
        return answer


def run_qa():
    QA = QAEngine()
    QA.run()
    #global writer
    #writer.close()
    QA.save_answers()

#############################################################


def main():
    #writer = tsv.TsvWriter(open("hw6-responses.tsv", "w"))
    #writer.line("answer","qid")
    run_qa()
    # You can uncomment this next line to evaluate your
    # answers, or you can run score_answers.py
    score_answers()

if __name__ == "__main__":
    main()
