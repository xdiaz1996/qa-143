import sys, nltk, operator, re
from qa_engine.base import QABase
from qa_engine.score_answers import main as score_answers
import tsv
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import wordnet

writer = tsv.TsvWriter(open("hw6-responses.tsv", "w"))
i = 0

exceptions = ['happen', 'travel', 'graze']
pronouns = ["I", "he", "He", "she", "She", "my", "My", "His", "his","hers", "Hers", "it", "It", "bull", "soup"]

# baseline funct
def get_sentences(text):
    sentences = nltk.sent_tokenize(text)
    sentences = [nltk.word_tokenize(sent) for sent in sentences]
    sentences = [nltk.pos_tag(sent) for sent in sentences]
    
    return sentences
#baseline funct
def get_bow(tagged_tokens, stopwords):
    return set([t[0].lower() for t in tagged_tokens if t[0].lower() not in stopwords])

#baseline funct
def baseline(qbow, sentences, stopwords):
    # Collect all the candidate answers
    answers = []
    for sent in sentences:
        # A list of all the word tokens in the sentence
        sbow = get_bow(sent, stopwords)
        
        # Count the # of overlapping words between the Q and the A
        # & is the set intersection operator
        overlap = len(qbow & sbow)
        
        answers.append((overlap, sent))
        
    # Sort the results by the first element of the tuple (i.e., the count)
    # Sort answers from smallest to largest by default, so reverse it
    answers = sorted(answers, key=operator.itemgetter(0), reverse=True)
    # Return the best answer
    #best_answer = (answers[0])[1]    
    best_answer = []
    for answer in answers:
        if(answer[0] == answers[0][0]):
            best_answer.extend(answer[1])
    return best_answer

#baseline funct
def find_phrase(tagged_tokens, qbow):
    for i in range(len(tagged_tokens) - 1, 0, -1):
        word = (tagged_tokens[i])[0]
        if word in qbow:
            return tagged_tokens[i+1:]
#-----Xavier const functions----------------------------------------------------------------#
def findVerbs(que):
    quesent = nltk.sent_tokenize(que)
    quewords = [nltk.word_tokenize(sent) for sent in quesent]
    quetagged = nltk.pos_tag(quewords[0])
    lmtzr = WordNetLemmatizer()
    verbs = []
    for word in quetagged:
        checkv = ""
        checkv = word[1]
        if checkv[:2] == "VB":
            word = lmtzr.lemmatize(word[0],"v")
            if (word == "be" or word == "do"):# no need to find syns of what and did
                verbs.append(word)
                continue
            wordsyns = wordnet.synsets(word)
            for syn in wordsyns:
                syno = syn.lemmas()[0].name()
                if syno == "feed":
                    syno = "graze"
                if syno in verbs:
                    continue
                verbs.append(syno)
        checkv = word[0]
        if checkv[-3:] == 'ing':
            verbs.append(lmtzr.lemmatize(word[0],"v"))
        elif checkv[-2:] == 'ed':
            verbs.append(lmtzr.lemmatize(word[0],"v"))
        elif checkv in exceptions:
            verbs.append(lmtzr.lemmatize(word[0],"v"))
    if verbs == []:
        return 'oops'
    else:
        return verbs

def findSubjs(que):
    quesent = nltk.sent_tokenize(que)
    quewords = [nltk.word_tokenize(sent) for sent in quesent]
    quetagged = nltk.pos_tag(quewords[0])
    lmtzr = WordNetLemmatizer()
    subjs = []
    for word in quetagged:
        checkv = ""
        checkv = word[0]
        if checkv[-3:] == 'ing':
            continue
        if checkv[-2:] == 'ed':
            continue
        if checkv in exceptions:
            continue
        checkv = word[1]
        if checkv[:2] == "NN":
            subjs.append(lmtzr.lemmatize(word[0], "n"))
        if (checkv[:3] == "PRP"):
            subjs.append(word[0])
        checkv = word[0]
        if checkv in pronouns:
            subjs.append(word[0])

    if subjs == []:
        return 'oops'
    else:
        return subjs

def findCandidates(verbs, subjs, sch_story):
    candsent = nltk.sent_tokenize(sch_story)
    vc = len(verbs)
    sc = len(subjs)
    lmtzr = WordNetLemmatizer()
    candidates = []
    i = 0
    for sent in candsent:
        vf = 0
        sf = 0
        words = nltk.word_tokenize(sent)
        print("\n")
        #print(format(sent))
        for word in words:
            if word[:1] == '.':
                word = word[1:]
            if word in pronouns:
                #print(word+" was in sujbs")
                sf +=1
                continue
            wordsl = lmtzr.lemmatize(word, "n")
            if wordsl in subjs:
                #print(wordsl+" was in subjs")
                sf += 1
                continue
            wordl = lmtzr.lemmatize(word, "v")
            if wordl in verbs:
                #print(wordl+" was in verbs")
                vf += 1
        if (vf >= 1 and sf >= 1):
            candidates.append(i)
        elif (vf >= 1 and sf == "oops"):
            candidates.append(i)
        i += 1
    return candidates
#-----I didn't write these------------------------------------------------------------------#
def pattern_matcher(pattern, tree):
    for subtree in tree.subtrees():
        node = matches(pattern, subtree)
        if node is not None:
            return node
    return None

def matches(pattern, root):
    # Base cases to exit our recursion
    # If both nodes are null we've matched everything so far
    if root is None and pattern is None:
        return root

    # We've matched everything in the pattern we're supposed to (we can ignore the extra
    # nodes in the main tree for now)
    elif pattern is None:
        return root

    # We still have something in our pattern, but there's nothing to match in the tree
    elif root is None:
        return None

    # A node in a tree can either be a string (if it is a leaf) or node
    plabel = pattern if isinstance(pattern, str) else pattern.label()
    rlabel = root if isinstance(root, str) else root.label()

    # If our pattern label is the * then match no matter what
    if plabel == "*":
        return root
    # Otherwise they labels need to match
    elif plabel == rlabel:
        # If there is a match we need to check that all the children match
        # Minor bug (what happens if the pattern has more children than the tree)
        for pchild, rchild in zip(pattern, root):
            match = matches(pchild, rchild)
            if match is None:
                return None
        return root

    return None

#-----Xavier const functions----------------------------------------------------------------#

def whoProc(question, answer):
    toksent = nltk.sent_tokenize(question)
    tokword = [nltk.word_tokenize(sent) for sent in toksent]
    modq = ""
    for word in tokword[0]:
        if word == "Who":
            continue
        if word == "?":
            continue
        modq = modq +" "+ str(word)
    whoregex = r"(.*)"+re.escape(modq)
    newans = re.findall(whoregex, answer)
    if newans == []:
        #answer = answer # do nothing 
        answer = answer.split()
        for word in tokword[0]:
            if word in answer:
                answer.remove(word)
        answer = " ".join(answer)
    else:
        answer = newans[0]
    return answer

def whereProc(question, answer):
    #essentialy looks for a
    toksent = nltk.sent_tokenize(question)
    tokword = [nltk.word_tokenize(sent) for sent in toksent]
    verb = ""
    tagged = nltk.pos_tag(tokword[0])
    for word in tagged:
        checkv = ""
        checkv = word[1]
        if checkv[:2] == "VB":
            verb = word[0]
        checkv = word[0]
        if checkv[-3:] == "ing":
            verb = word[0]
        if checkv[-2:] == "ed":
            verb = word[0]
    if verb == "":
        return answer
    lmtzr = WordNetLemmatizer()
    verb = lmtzr.lemmatize(verb, "v")
    ansent = nltk.sent_tokenize(answer)
    shorta = ""
    i = 0
    for sent in ansent:
        tokwrd = [nltk.word_tokenize(sent) for sent in ansent]
        tag = nltk.pos_tag(tokwrd[i])
        for word in tag:
            cmatch = ""
            cmatch = lmtzr.lemmatize(word[0], "v")
            if cmatch == verb:
                shorta = shorta +" "+sent
                break
        i = i + 1
    #print(question+"\n")
    #print(answer+"\n")
    #print(shorta)
    #sys.exit(0)
    if shorta == "":
        #answer = answer# do nothing
        answer = answer.split()
        for word in tokword[0]:
            if word in answer:
                answer.remove(word)
        answer = " ".join(answer)
    else:
        #answer = shorta
        answer = shorta.split()
        for word in tokword[0]:
            if word in answer:
                answer.remove(word)
        answer = " ".join(answer)
    return answer

def whatProc(question, answer):
    #essentialy looks for a
    toksent = nltk.sent_tokenize(question)
    tokword = [nltk.word_tokenize(sent) for sent in toksent]
    verb = ""
    tagged = nltk.pos_tag(tokword[0])
    for word in tagged:
        checkv = ""
        checkv = word[1]
        if checkv[:2] == "VB":
            verb = word[0]
        if checkv[:2] == "NN":
            verb = word[0]
        checkv = word[0]
        if checkv[-3:] == "ing":
            verb = word[0]
        if checkv[-2:] == "ed":
            verb = word[0]
    if verb == "":
        return answer
    lmtzr = WordNetLemmatizer()
    verb = lmtzr.lemmatize(verb, "v")
    ansent = nltk.sent_tokenize(answer)
    shorta = ""
    i = 0
    for sent in ansent:
        tokwrd = [nltk.word_tokenize(sent) for sent in ansent]
        tag = nltk.pos_tag(tokwrd[i])
        for word in tag:
            cmatch = ""
            cmatch = lmtzr.lemmatize(word[0], "v")
            if cmatch == verb:
                shorta = shorta +" "+sent
                break
        i = i + 1
    print(question+"\n")
    print(answer+"\n")
    print(shorta)
    if shorta == "":
        #answer = answer# do nothing
        answer = answer.split()
        for word in tokword[0]:
            if word in answer:
                answer.remove(word)
        answer = " ".join(answer)
    else:
        #answer = shorta
        answer = shorta.split()
        for word in tokword[0]:
            if word in answer:
                answer.remove(word)
        answer = " ".join(answer)
    return answer

def whenProc(question, answer):
    #essentialy looks for a
    toksent = nltk.sent_tokenize(question)
    tokword = [nltk.word_tokenize(sent) for sent in toksent]
    verb = ""
    tagged = nltk.pos_tag(tokword[0])
    for word in tagged:
        checkv = ""
        checkv = word[1]
        if checkv[:2] == "VB":
            verb = word[0]
        checkv = word[0]
        if checkv[-3:] == "ing":
            verb = word[0]
        if checkv[-2:] == "ed":
            verb = word[0]
    if verb == "":
        return answer
    lmtzr = WordNetLemmatizer()
    verb = lmtzr.lemmatize(verb, "v")
    ansent = nltk.sent_tokenize(answer)
    shorta = ""
    i = 0
    for sent in ansent:
        tokwrd = [nltk.word_tokenize(sent) for sent in ansent]
        tag = nltk.pos_tag(tokwrd[i])
        for word in tag:
            cmatch = ""
            cmatch = lmtzr.lemmatize(word[0], "v")
            if cmatch == verb:
                shorta = shorta +" "+sent
                break
        i = i + 1
    #print(question+"\n")
    #print(answer+"\n")
    #print(shorta)
    if shorta == "":
        #answer = answer# do nothing
        answer = answer.split()
        for word in tokword[0]:
            if word in answer:
                answer.remove(word)
        answer = " ".join(answer)
    else:
        #answer = shorta
        answer = shorta.split()
        for word in tokword[0]:
            if word in answer:
                answer.remove(word)
        answer = " ".join(answer)
    return answer

def get_answer(question, story):
    """
    :param question: dict
    :param story: dict
    :return: str


    question is a dictionary with keys:
        dep -- A list of dependency graphs for the question sentence.
        par -- A list of constituency parses for the question sentence.
        text -- The raw text of story.
        sid --  The story id.
        difficulty -- easy, medium, or hard
        type -- whether you need to use the 'sch' or 'story' versions
                of the .
        qid  --  The id of the question.


    story is a dictionary with keys:
        story_dep -- list of dependency graphs for each sentence of
                    the story version.
        sch_dep -- list of dependency graphs for each sentence of
                    the sch version.
        sch_par -- list of constituency parses for each sentence of
                    the sch version.
        story_par -- list of constituency parses for each sentence of
                    the story version.
        sch --  the raw text for the sch version.
        text -- the raw text for the story version.
        sid --  the story id


    """
    ###     Your Code Goes Here         ###
    answer = 1
    # Xavier const stuff-----------------------------------------------------#
    stry = False
    qid = format(question['qid'])
    que = question["text"]
    version = question["type"]
    quetype = ""
    if que[1:5] == "here":
        quetype = "where"
    elif que[1:3] == "ho":
        quetype = "who"
    if (quetype == "who" or quetype == "where"):
        #print(quetypei)
        #print(que)
        verbs = findVerbs(que)
        #was and do aren't very useful verbs if others are present, if more than 1 remove be
        better = []
        if len(verbs) > 1:
            for verb in verbs:
                if verb == 'be':
                    continue
                elif verb == 'do':
                    continue
                else:
                    better.append(verb)
            verbs = better
        subjs = findSubjs(que)
        #print("subjs: "+format(subjs))
        #print("verbs: "+format(verbs))
        # put the sch_par together into a story to look over

        par_len = len(story["sch_par"])
        j = 0
        par_story = ""
        while j < par_len and version != "Story | Sch":
            a = " ".join(story["sch_par"][j].leaves())
            a = str(a[:-2])+"."
            if len(a) < 3:
                continue
            par_story += " "+a
            j += 1
        par_story = par_story[1:]
        if par_story == "":
            stry = True
            par_len = len(story["story_par"])
            j = 0
            par_story = ""
            while j < par_len:
                a = " ".join(story["story_par"][j].leaves())
                a = str(a[:-2])+"."
                if len(a) < 3:
                    continue
                par_story += " "+a
                j += 1
            par_story = par_story[1:]
        #print(format(par_story))
        #now have the story to look through, pass it with verbs and subjs
        candidates = findCandidates(verbs, subjs, par_story)
        #print(format(candidates))

        #can now iterate over candidates and try to get the answer
        answers = []
        tree = ""
        # if stry == True:
        for cand in candidates:
            if stry == True:
                if cand >= len(story["story_par"]):
                    break
                tree = story["story_par"][cand]
            else:
                if cand >= len(story["sch_par"]):
                    break
                tree = story["sch_par"][cand]
            #print(format(tree))
            if quetype == "where":
                pattern = nltk.ParentedTree.fromstring("(VP)")#for where
            if quetype == "who":
                pattern = nltk.ParentedTree.fromstring("(NP)")#for who
                pattern2 = nltk.ParentedTree.fromstring("(VP)")#for who
            if quetype == "who":
                subtree = pattern_matcher(pattern, tree)
                subtreetwo = pattern_matcher(pattern2, tree)
            else:
                subtree = pattern_matcher(pattern, tree)
            if quetype == "who":
                pattern = nltk.ParentedTree.fromstring("(NP)")#for who
            if quetype == "where":
                pattern = nltk.ParentedTree.fromstring("(PP)")#for where
            if quetype == "who":
                subtree2 = pattern_matcher(pattern, subtree)
                subtree3 = pattern_matcher(pattern, subtreetwo)
            else:
                subtree2 = pattern_matcher(pattern, subtree)
            if subtree2 == None:
                continue
            if quetype == "who":
                if subtree3 == None:
                    continue
            if quetype == "where":
                answers.append(" ".join(subtree2.leaves()))
            elif quetype == "who":
                answers.append(" ".join(subtree2.leaves()))
                answers.append(" ".join(subtree3.leaves()))
            #print(format(answers))
            answer = str(" ".join(answers))
            #if answer != "":
            #    print(answer)

    # end of constit logic---------------------------------------------------#
    #-baseline method-------------------------------------------
    if answer == 1:
        answer = []
        version = question["type"]
        qid = format(question['qid'])
        if version == "Story":
            text = story["text"]
        else:
            text = story["sch"]
        question = question["text"]
        #print("question: ", question+"\n")
        stopwords = set(nltk.corpus.stopwords.words("english"))
        qbow = get_bow(get_sentences(question)[0], stopwords)
        sentences = get_sentences(text)
        answer_stuff = baseline(qbow, sentences, stopwords)
        for t in answer_stuff:
            answer.append(t[0])
        answer = " ".join(answer)
        if question[:3] == "Who":
            answer = whoProc(question, answer)
        elif question[:5] == "Where":
            answer = whereProc(question, answer)
        elif question[:4] == "What":
            answer = whatProc(question, answer)
        elif question[:4] == "When":
            answer = whenProc(question, answer)
        no_repeats = []
        answer = answer.split()
        for word in answer:
            if word not in no_repeats:
                no_repeats.append(word)
        answer = " ".join(no_repeats)
        global writer
        writer.line(answer, qid)
    else:
        print("solution was found above at: "+str(qid))
        #print("answer: "+answer)
        #print("answer: "," ".join(t[0]for t in answer_stuff))
        #global writer
        #writer.line(answer, qid)
        no_repeats = []
        answer = answer.split()
        for word in answer:
            if word not in no_repeats:
                no_repeats.append(word)
        answer = " ".join(no_repeats)

        global writer
        writer.line(answer, qid)
    #-end of baseline method--------------------------------------
    #global writer
    #writer.line(answer, qid)
    ###     End of Your Code         ###
    #words = string1.split()
    #answers = " ".join(sorted(set(words), key=words.index))
    return answer



#############################################################
###     Dont change the code in this section
#############################################################
class QAEngine(QABase):
    @staticmethod
    def answer_question(question, story):
        answer = get_answer(question, story)
        return answer


def run_qa(evaluate=False):
    QA = QAEngine(evaluate=evaluate)
    QA.run()
    global writer
    writer.close()
    #QA.save_answers()

#############################################################


def main():
    #writer = tsv.TsvWriter(open("hw6-responses.tsv", "w"))
    writer.line("answer","qid")
    run_qa(evaluate=True)
    # You can uncomment this next line to evaluate your
    # answers, or you can run score_answers.py
    score_answers()

if __name__ == "__main__":
    main()
